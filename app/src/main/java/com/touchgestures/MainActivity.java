package com.touchgestures;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.TouchDelegate;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {
    private Matrix matrix = new Matrix();
    private float scale = 1f;
    private ScaleGestureDetector SGD;
    ImageView myButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SGD = new ScaleGestureDetector(this, new ScaleListener());
        myButton = (ImageView) findViewById(R.id.imageView);
        myButton.setOnTouchListener(this);
//        // Get the parent view
        View parentView = findViewById(R.id.parent_layout);

        parentView.post(new Runnable() {
            // Post in the parent's message queue to make sure the parent
            // lays out its children before you call getHitRect()
            @Override
            public void run() {
                // The bounds for the delegate view (an ImageButton
                // in this example)
                Rect delegateArea = new Rect();

                myButton.setEnabled(true);
                myButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(MainActivity.this,
                                "Touch occurred within ImageButton touch region.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                myButton.getHitRect(delegateArea);
                delegateArea.right += 100;
                delegateArea.bottom += 100;
                TouchDelegate touchDelegate = new TouchDelegate(delegateArea,
                        myButton);
                if (View.class.isInstance(myButton.getParent())) {
                    ((View) myButton.getParent()).setTouchDelegate(touchDelegate);
                }
            }
        });
    }

    public boolean onTouchEvent(MotionEvent ev) {
        SGD.onTouchEvent(ev);
        return true;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int action = MotionEventCompat.getActionMasked(event);

        switch (action) {
            case (MotionEvent.ACTION_DOWN):
                Toast.makeText(MainActivity.this,
                        "Action was DOWN",
                        Toast.LENGTH_SHORT).show();

                return true;
            case (MotionEvent.ACTION_MOVE):
                Toast.makeText(MainActivity.this,
                        "Action was MOVE",
                        Toast.LENGTH_SHORT).show();
                return true;
            case (MotionEvent.ACTION_UP):
                Toast.makeText(MainActivity.this,
                        "Action was UP",
                        Toast.LENGTH_SHORT).show();

                return true;
            case (MotionEvent.ACTION_CANCEL):
                Toast.makeText(MainActivity.this,
                        "Action was CANCEL",
                        Toast.LENGTH_SHORT).show();

                return true;
            case (MotionEvent.ACTION_OUTSIDE):
                Toast.makeText(MainActivity.this,
                        "Movement occurred outside bounds " +
                                "of current screen element",
                        Toast.LENGTH_SHORT).show();

                return true;
            default:
                return super.onTouchEvent(event);
        }
    }

    private class ScaleListener extends ScaleGestureDetector.
            SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scale *= detector.getScaleFactor();
            scale = Math.max(0.1f, Math.min(scale, 5.0f));
            matrix.setScale(scale, scale);
            myButton.setImageMatrix(matrix);
            return true;
        }
    }
}
